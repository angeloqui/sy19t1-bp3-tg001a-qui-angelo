#pragma once

#include <string>
#include <sstream>
#include <vector>

using namespace std;

class Character;
class Skill {
public:
	Skill(string name);
	~Skill();

	virtual void cast(Character* target, Character* caster);
	virtual void cast(vector<Character*> target, Character* caster);
	
	float bonusDamage(Character* target, Character* caster);
	
	int calculateFinalDamage(string name, Character* target, Character* caster, float damageCoefficient);
	int calculateHitRate(Character* target, Character* caster);
	
	string getName();
	int getMPCost();
protected:
	string name;
	int mpCost;
};
