#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "Character.h"

using namespace std;

class Team {
public:
	Team(string name);
	~Team();
	
	void addMember(Character* character);
	vector<Character*> getMember();
	
	void displayName();
	void displayMembers();
	int isAlive();
private:
	string name;
	vector<Character*> members;
};
