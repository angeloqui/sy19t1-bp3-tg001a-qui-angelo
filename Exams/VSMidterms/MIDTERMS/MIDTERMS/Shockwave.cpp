#include "Shockwave.h"
#include "Character.h"

Shockwave::Shockwave(string name) : Skill(name) {
}

Shockwave::~Shockwave() {
}

void Shockwave::cast(vector<Character*> target, Character* caster) {	
	int totalDamage = 0;
	for (int i = 0; i < target.size(); i++) {
		int damage = calculateFinalDamage(name, target[i], caster, 0.9f);
		target[i]->takeDamage(damage);
		totalDamage += damage;
	}
	stringstream ss;
	ss << totalDamage;
	string nlog = caster->getName() + " used " + name + "! it dealt " + ss.str() + " total damage to the enemy team";
	caster->setLog(nlog);
	
	caster->reduceMP(mpCost);
}
