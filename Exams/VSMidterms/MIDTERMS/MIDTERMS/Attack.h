#pragma once

#include "Skill.h"

#include <string>
#include <sstream>

class Attack : public Skill {
public: 
	Attack(string name);
	~Attack();
	
	void cast(Character* target, Character* caster);
};
