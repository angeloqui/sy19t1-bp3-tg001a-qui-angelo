#include "jRPG.h"

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_ENTER 13

int AGILITY_INDEX = 4;
int CHARACTERS_SIZE = 6;
int LOG_SPACING = 2;

jRPG::jRPG() {
	teams.resize(CHARACTERS_SIZE);
	log.resize(CHARACTERS_SIZE);

	teamNumber = 0;
	option = 0;
	option2 = 0;
	hasSelected = 0;
	hasTarget = 0;
}

jRPG::~jRPG() {
}

void jRPG::addMember(Team* team) {
	
	//add character reference from teams
	vector<Character*> vc = team->getMember();
	
	Character* temp;
	for (int i = 0; i < vc.size(); i++) {
		members.push_back(vc[i]);
		teams[teamNumber].push_back(vc[i]);
		for (int j = members.size() - 1; j > 0; j--) {
			if (members[j]->getStats()[AGILITY_INDEX] > members[j - 1]->getStats()[AGILITY_INDEX]) {
				temp = members[j];
				members[j] = members[j - 1];
				members[j - 1] = temp;
			}
		}
	}
	
	teamNumber++;
}

Character* jRPG::getMember() {
	return members[0];
}

void jRPG::attackPhase() {	

	//get current character skill reference
	skill = members[0]->getSkills();
	
	//Check if current character is inside a player team
	//prevents friendly fire
	int n;
	for (int i = 0; i < 3; i++) {
		if (members[0]->getName() == teams[0][i]->getName()) {
			n = 1;
			break;
		} else n = 0;
	}
	
	displayPhase();
	
	//Display options UI
	if (n == 1) {
		//Current character stats display
		cout << string(2, '\n') << "[" << members[0]->getName() << "] [MP: " << members[0]->getStats()[1] << "]"
			 << "\nOptions: " << endl;
		
		//Getting current character skills display
		for (int i = 0; i < skill.size() + 1; i++) {
			if (option == i) 
				cout << "-> ";
			if (i < skill.size()) {
				cout << skill[i]->getName();
			}
					
			else cout << string(10, ' ');
			
			//Display targets if player has selected Attack
			if (hasSelected == true && option == 0) {
				cout << string(2, ' ') << "\t\t";
				if (option2 == i) 
					cout << "-> ";
				if (i < teams[1].size()) {
					cout << teams[1][i]->getName();
					displayWeakness(members[0]->getJob(), teams[1][i]->getJob());
				}
			 //If uses a skill, dont display any target
			} else if (option == 1) {
				hasTarget = true;
			}
			cout << endl;
		}
	} else { //If not player
		//Set skill & target for enemy AI
		cout << "\n\nENEMY TURN...";
		hasTarget = true;
		if (members[0]->getStats()[1] >= members[0]->getSkills()[1]->getMPCost()) option = rand() % 2;
		else option = 0;
		option2 = rand() % teams[0].size();
	}
	
	//option = skill selected, option2 = target selected
	if (hasSelected == false) {
		hasSelected = controlInput(this->option);
		hasTarget = false;
	}
	else if (hasSelected == true) {
		if (hasTarget != true) hasTarget = controlInput(this->option2);
		if (hasTarget == true && members[0]->getStats()[1] < members[0]->getSkills()[1]->getMPCost() && option == 1) {
			//if has no mana, return to selection
			hasTarget = false;
			hasSelected = false;
			log.insert(log.begin(), "Not enough mana!");
		}
	}
	
	if (hasTarget == true && hasSelected == true) {			
		if (option == 1) {
			if (members[0]->getJob() == 2) {
				if (n == 0) n = 1;
				else n = 0;
			}
		}
		
		vector<Character*> targetTeam;
		Character* temp;
		for (int i = 0; i < teams[n].size(); i++) {
			targetTeam.push_back(teams[n][i]);
			for (int j = targetTeam.size() - 1; j > 0; j--) {
				if (targetTeam[j]->getStats()[0] < targetTeam[j - 1]->getStats()[0]) {
					temp = targetTeam[j];
					targetTeam[j] = targetTeam[j - 1];
					targetTeam[j - 1] = temp;
				}
			}
		}
		
		if (option == 0) { 
			//if single target
			skill[option]->cast(teams[n][option2], members[0]);
		} else if (option == 1) { 
			//if target is a team, also works in assassinate & heal, sorted as the first
			//index of the vertex has the lowest hp
			skill[option]->cast(targetTeam, members[0]);
		}

		log.insert(log.begin(), members[0]->getLog());
		
		rotatePhase();

		//end turn, reset all
		option = 0;
		option2 = 0;
		hasSelected = 0;
		hasTarget = 0;
	}
}

void jRPG::rotatePhase() {
	//remove dead characters before rotating
	for (int i = 0; i < members.size(); i++) {		
		if (members[i]->isAlive() != true) {			
			for (int j = 0; j < 2; j++) {
				for (int k = 0; k < teams[j].size(); k++) {
					if (members[i]->getName() == teams[j][k]->getName()) {
						rotate(teams[j].begin() + k, teams[j].begin() + k + 1, teams[j].end());
						teams[j].pop_back();
					}
				}
			}
			rotate(members.begin() + i, members.begin() + i + 1, members.end());
			members.pop_back();	
		}
	}
	
	rotate(members.begin(), members.begin() + 1, members.end());
	log.resize(CHARACTERS_SIZE);
		
}

void jRPG::displayPhase() {
	cout << "\n\tAttack Phase" << string(5, '\t') << "Battle Log" << endl;
	UIborders();
	
	for (int i = 0; i < 6; i++) {		
		if (i < members.size()) displayMembers(i);
		else cout << string(3, '\t');
		displayLog(i);
	}
}

void jRPG::displayLog(int n) {
	cout << string(4, ' ') << string(LOG_SPACING, '\t');
	if (n == 0) cout << "> ";
	cout << log[n] << endl;
}

void jRPG::displayMembers(int n) {
	cout << "[";
		 if (members[n]->getJob() == 0)	cout << "W";
	else if (members[n]->getJob() == 1) cout << "A";
	else if	(members[n]->getJob() == 2)	cout << "M";
	cout << "] ";
	cout << members[n]->getName();
}

void jRPG::UIborders() {
	cout << string(30, '=') << string(LOG_SPACING, '\t') << string(30, '=') << endl;
}

bool jRPG::controlInput(int& option) {
	int key;
	switch((key = _getch())) {
    case KEY_UP:
		option--;
		optionBound();
		return false;
    case KEY_DOWN:
        option++;
        optionBound();
		return false;
    case KEY_ENTER:
    	return true;
	default:
		return false;
    }
}

void jRPG::optionBound() {
	//for some reason if (option > skill.size() - 1) does not
	//work properly on my IDE. so i seperated it and used another
	//temp variable called skillSize
	int skillSize = skill.size() - 1;
	
	if (option > skillSize) {
		option = 0;
	} else if (option < 0) {
		option = skillSize;
	}
}

void jRPG::displayWeakness(int caster, int target) {
	if (caster == 0 && target == 1) cout << "**";
	else if (caster == 1 && target == 2) cout << "**";
	else if (caster == 2 && target == 0) cout << "**";
}
