#include "Character.h"

int STATS_MODIFIER = 5;

Character::Character(string name, int job, int stats[]) {
	this->name = name;
	this->job = job;
	
	//Stats modifier
	for (int i = 0; i < 6; i++) {
		this->stats[i] = rand() % STATS_MODIFIER + stats[i];
	}
	
	//Add skills
	skill.push_back(new Attack("Attack"));
	
	if (job == 0) {
		skill.push_back(new Shockwave("Shockwave"));
	} else if (job == 1) {
		skill.push_back(new Assassinate("Assassi...")); //because of display
	} else if (job == 2) {
		skill.push_back(new Heal("Heal.."));
	}
}

Character::~Character() {
	for (int i = 0; i < skill.size(); i++) {
		delete skill[i];
	}
	skill.clear();
}

string Character::getName() {
	return name;
}

int* Character::getStats() {
	return stats;
}

int Character::getJob() {
	return job;
}

vector<Skill*> Character::getSkills() {
	return skill;
}

string Character::getLog() {
	return log;
}

void Character::setLog(string log) {
	this->log = log;
}

void Character::takeDamage(int value) {
	if (value > 0) {
		stats[0] -= value;

		if (stats[0] < 0) stats[0] = 0;
	}
}

void Character::reduceMP(int value) {
	if (value > 0) {
		stats[1] -= value;

		if (stats[1] < 0) stats[1] = 0;
	}	
}

void Character::heal(int value) {
	if (value > 0) {
		stats[0] += value;
	}
}

bool Character::isAlive() {
	return stats[0] > 0;
}
