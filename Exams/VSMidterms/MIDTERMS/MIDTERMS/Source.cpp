#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> 

using namespace std;

//for Bloodshed IDE
#include "jRPG.h"
#include "Team.h"
#include "Character.h"
#include "Skill.h"
#include "Attack.h"
#include "Shockwave.h"
#include "Assassinate.h"
#include "Heal.h"

#include "jRPG.cpp"
#include "Team.cpp"
#include "Character.cpp"
#include "Skill.cpp"
#include "Attack.cpp"
#include "Shockwave.cpp"
#include "Assassinate.cpp"
#include "Heal.cpp"

int main() {
	
	//Intro
	string ASCII_template = 
	"	 __      _____  ___ _    ___     ___  ___  __      ___   ___  ___ ___    _   ___ _____ \n"
	"	 \\ \\    / / _ \\| _ \\ |  |   \\   / _ \\| __| \\ \\    / /_\\ | _ \\/ __| _ \\  /_\\ | __|_   _|\n"
	"	  \\ \\/\\/ / (_) |   / |__| |) | | (_) | _|   \\ \\/\\/ / _ \\|   / (__|   / / _ \\| _|  | |  \n"
	"	   \\_/\\_/ \\___/|_|_\\____|___/   \\___/|_|___  \\_/\\_/_/_\\_\\_|_\\\\___|_|_\\/_/ \\_\\_|   |_|  \n"
	"	                    ___  | |    | __|  / __| |_ _|  / _ \\  | \\| |  ___                 \n"
	"	                   |___| | |__  | _|  | (_ |  | |  | (_) | | .` | |___|                \n"
	"	                         |____| |___|  \\___| |___|  \\___/  |_|\\_|                      \n";
	                                                                                       
	cout << ASCII_template;
	cout << "[play at MAXSCREEN]";
	
	cout << string(3, '\n');
	
	cout << "\nIllidan Stormrage:"
		 << "\nYou are here Champion! the hour has come at last."
		 << "\nI will finish my hunt and the Legion will meet its end!" << endl;
	
	cout << "\nControls:" 
		 << "\nARROW KEYS"
		 << "\nENTER";
		
	_getch();
	system("cls");
	
	//Midterms Start
	string teamName[2] = {"Azeroth", "Legion"};
	string memberName[2][3] = {{"Illidan Stormrage", "Sylvanas Windrunner", "King Anduin Wrynn"}, 
							   {"Argus the Unmaker", "Sargeras the Dark Titan", "Kil'jaeden the Deciever"}};
	int stats[6] = {100, 100, 10, 10, 10, 10}; //has random modifier in Character class
	//

	//Initialize random seed
	srand (time(NULL));
	
	Team* team[2];
	for (int i = 0; i < 2; i++) {
		//Create new Team
		team[i] = new Team(teamName[i]);
		
		//Add new Members
		for (int j = 0; j < 3; j++) {
			team[i]->addMember(new Character(memberName[i][j], j, stats));
		}
	}
	
	//Create Battle Logic
	jRPG *jrpg = new jRPG();
	for (int i = 0; i < 2; i++) {
		jrpg->addMember(team[i]);
	}
	
	//Game
	while (team[0]->isAlive() && team[1]->isAlive()) {
		//Display UI
		for (int i = 0; i < 2; i++) {
			team[i]->displayName();
			team[i]->displayMembers();
		}
		
		jrpg->attackPhase();
		system("cls");
	}
	
	if (team[0]->isAlive()) {
		
		cout << string(30, '=') << endl;
		cout << "THE LEGION IS DEFEATED!" << endl;
		cout << string(30, '=') << endl;
		
		cout << "Prophet Velen: "
			 << "\nIllidan, we've done all we can" << endl;
		cout << "\nIllidan Stormrage: "
			 << "\nEvery choice, every sacrifice has led me to this moment. To face him once again" << endl;
		cout << "\nProphet Velen: "
			 << "\nYou are not coming with us..." << endl;
		cout << "\nIllidan Stormrage: "
			 << "\nThe hunter is nothing without the hunt, did you not see this fate prophet?" << endl;
		cout << "\nProphet Velen: "
			 << "\nFate.. Our survival was never in fate's hands" << endl;
		
		cout << "\n\nProphet Velen: "
			 << "\nLight be with you Illidan Stormrage" << endl;
	} else {
		cout << "You have failed Azeroth, you are not worthy of an epic ending champion..";
	}
	
	cout << endl;
	system("pause");
	
	//Memory Cleanup
	delete[] team;
	delete jrpg;
	return 0;
}
