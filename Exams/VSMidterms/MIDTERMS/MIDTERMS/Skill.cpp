#include "Skill.h"
#include "Character.h"

Skill::Skill(string name) {
	this->name = name;
	mpCost = 30;
}

Skill::~Skill() {
}

void Skill::cast(Character* target, Character* caster) {
}

void Skill::cast(vector<Character*> target, Character* caster) {
}


float Skill::bonusDamage(Character* target, Character* caster) {
	if (caster->getJob() == 0 && target->getJob() == 1) return 1.5;
	else if (caster->getJob() == 1 && target->getJob() == 2) return 1.5;
	else if (caster->getJob() == 2 && target->getJob() == 0) return 1.5;
	else return 1;
}

int Skill::calculateFinalDamage(string name, Character* target, Character* caster, float damageCoefficient) {
	int baseDamage = rand() % caster->getStats()[2] + (caster->getStats()[2] * 1.2f) * damageCoefficient;
	int damage = (baseDamage - target->getStats()[3]) * bonusDamage(target, caster);
	if (damage < 0) damage = 0;
	
	//store string in log
	stringstream ss;
	ss << damage;
	string nlog = caster->getName() + " used " + name + "! it dealt " + ss.str() + " damage to " + target->getName();
	caster->setLog(nlog);
		
	return damage;
}

int Skill::calculateHitRate(Character* target, Character* caster) {
	int hit = ((float) caster->getStats()[5] / (float)target->getStats()[4]) * 100;
	
	//clamp, not going to use additional libraries
	if (hit < 20) hit = 20;
	else if (hit < 80) hit = 80;
	
	if (rand() % 100 + 1 < hit) {
		hit = 1;
	} else {
		hit = 0;
		string nlog = caster->getName() + " missed!";
		caster->setLog(nlog);
	}
	
	return hit;
}

string Skill::getName() {
	return name;
}

int Skill::getMPCost() {
	return mpCost;
}
