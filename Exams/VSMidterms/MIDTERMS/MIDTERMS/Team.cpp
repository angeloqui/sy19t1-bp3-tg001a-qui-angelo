#include "Team.h"

Team::Team(string name) {
	this->name = name;
}

Team::~Team() {
	for (int i = 0; i < members.size(); i++) {
		delete members[i];
	}
	members.clear();
}

void Team::addMember(Character* character) {		
	members.push_back(character);
}

vector<Character*> Team::getMember() {
	return members;
}

void Team::displayName() {
	cout << string(4, '\t') << name << endl;
}

void Team::displayMembers() {
	string statsName[6] = {"HP", "MP", "POW", "VIT", "AGI", "DEX"};
	
	cout << string(75, '=') << endl;
	for (int i = 0; i < members.size(); i++) {	
		cout << members[i]->getName() << "\t";
		
		if (members[i]->isAlive()) {
			int* n = members[i]->getStats();
			for (int j = 0; j < 5; j++) {
				cout << " [" << statsName[j] << ": " << *(n + j) << "]";
			}
		} else {
			cout << " [DEAD]";
		}
		cout << endl;
	}
	cout << string(2, '\n');
}

int Team::isAlive() {
	int counter = 3;
	for (int i = 0; i < 3; i++) {
		if (members[i]->isAlive() != true) {
			counter--;
		}
	}
	
	return counter;
}
