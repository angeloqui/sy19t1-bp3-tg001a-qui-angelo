#pragma once

#include "Team.h"
#include "Character.h"

#include <vector>
#include <algorithm>
#include <conio.h>

using namespace std;

class jRPG {
public:
	jRPG();
	~jRPG();
	
	void addMember(Team* team);
	Character* getMember();
	
	//phases
	void attackPhase();
	void rotatePhase();
	
	//ui
	void displayPhase();
	void displayLog(int n);
	void displayMembers(int n);
	
	void UIborders();
	
	//custom controls
	bool controlInput(int& option);
	void optionBound();
	
	//etc
	void displayWeakness(int caster, int target);
private:
	vector<Character*> members; //linear
	vector<vector<Character*> > teams; //2D vector
	vector<string> log;
	//stored reference skill of current character
	//prevent double call: teams()->getSkill()->cast;
	vector<Skill*> skill;
	int teamNumber;	
	
	//for option & selection
	int option;
	int option2;
	bool hasSelected;
	bool hasTarget;

};
