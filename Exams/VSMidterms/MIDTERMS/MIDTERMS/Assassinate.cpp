#include "Assassinate.h"
#include "Character.h"

Assassinate::Assassinate(string name) : Skill(name) {
}


Assassinate::~Assassinate() {
}

void Assassinate::cast(vector<Character*> target, Character* caster) {	
	int damage = calculateFinalDamage(name, target[0], caster, 2.2);
	target[0]->takeDamage(damage);
	
	caster->reduceMP(mpCost);
}
