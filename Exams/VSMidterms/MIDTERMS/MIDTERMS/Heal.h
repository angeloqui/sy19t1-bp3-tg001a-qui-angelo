#pragma once

#include "Skill.h"

#include <string>

class Heal : public Skill {
public: 
	Heal(string name);
	~Heal();
	
	void cast(vector<Character*> target, Character* caster);
};
