#pragma once

#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>

//Skills
#include "Attack.h"
#include "Shockwave.h"
#include "Assassinate.h"
#include "Heal.h"

class Skill;
class Character {
public:
	Character(string name, int job, int stats[]);
	~Character();
	
	string getName();
	int* getStats();
	int getJob();
	vector<Skill*> getSkills();
	
	string getLog();
	void setLog(string log);
	
	void takeDamage(int value);
	void reduceMP(int value);

	void heal(int value);
	bool isAlive();
private:
	string name;
	string log;
	int job;
	int stats[6];
	vector<Skill*> skill;
};
