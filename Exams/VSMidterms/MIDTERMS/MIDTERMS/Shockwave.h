#pragma once

#include "Skill.h"

#include <string>
#include <vector>

class Shockwave : public Skill {
public: 
	Shockwave(string name);
	~Shockwave();
	
	void cast(vector<Character*> target, Character* caster);
};
