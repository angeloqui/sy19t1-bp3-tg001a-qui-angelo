#include "Heal.h"
#include "Character.h"

Heal::Heal(string name) : Skill(name) {
}


Heal::~Heal() {
}

void Heal::cast(vector<Character*> target, Character* caster) {	
	int healAmount = target[0]->getStats()[0] * 0.3;
	target[0]->heal(healAmount);
	
	stringstream ss;
	ss << healAmount;
	string nlog = caster->getName() + " used " + name + "! it healed " + ss.str() + " health to " + target[0]->getName();
	caster->setLog(nlog);
	
	caster->reduceMP(mpCost);
}
