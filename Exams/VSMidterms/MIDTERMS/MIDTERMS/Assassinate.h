#pragma once

#include "Skill.h"

#include <string>

class Assassinate : public Skill {
public: 
	Assassinate(string name);
	~Assassinate();
	
	void cast(vector<Character*> target, Character* caster);
};
