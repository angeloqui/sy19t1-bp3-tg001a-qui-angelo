#include "Attack.h"
#include "Character.h"

Attack::Attack(string name) : Skill(name) {
}


Attack::~Attack() {
}

void Attack::cast(Character* target, Character* caster) {	
	int damage = calculateFinalDamage(name, target, caster, 1);
	//critical chance
	if (rand() % 100 + 1 <= 20) {
		damage * 1.2f;
		stringstream ss;
		ss << damage;
		string nlog = caster->getName() + " used " + name + "! [CRITICAL HIT] it dealt " + ss.str() + " damage to " + target->getName();
		caster->setLog(nlog);
	}
	
	int hit = calculateHitRate(target, caster);
	
	target->takeDamage(damage * hit);
}
