#include "Planet.h"
ManualObject* Planet::object = NULL;

Planet::Planet(SceneNode * node) {
	this->mNode = node;
}

Planet::~Planet(){
}

void Planet::Update(const FrameEvent& evt) {
	float rotSpd = 24 / mLocalRotationSpeed;
	float revSpd = (365 / mRevolutionSpeed);
	Degree rot = Degree(60 * evt.timeSinceLastFrame * rotSpd);
	mNode->rotate(Vector3(0, 1, 0), Radian(rot));
	if (mParent != NULL ) {
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * revSpd);
		Vector3 location = Vector3::ZERO;
		location.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		location.z = mNode->getPosition().z - mParent->mNode->getPosition().z;
		float oldX = location.x;
		float oldZ = location.z;
		float newX = (oldX*Math::Cos(planetRevolution)) + (oldZ*Math::Sin(planetRevolution));
		float newZ = (oldX*-Math::Sin(planetRevolution)) + (oldZ*Math::Cos(planetRevolution));
		mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}
}

void Planet::moonRotate(const FrameEvent& evt) {
	float rotSpd = 24 / mLocalRotationSpeed;
	float revSpd = (365 / mRevolutionSpeed);
	Degree rot = Degree(60 * evt.timeSinceLastFrame * rotSpd);
	mNode->rotate(Vector3(0, 1, 0), Radian(rot));
}

SceneNode & Planet::getNode() {
	return *mNode;
}

ManualObject* Planet::getObject() {
	return object;
}

void Planet::setPosition(Vector3 vec)
{
	mNode->setPosition(vec);
}

void Planet::setParent(Planet * parent) {
	mParent = parent;
}

void Planet::setLocalRotationSpeed(float speed) {
	 mLocalRotationSpeed = speed;
}	

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}


Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour, string str, bool light) {
	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode(str);
	object = Planet::createSphere(sceneManager, str, size, colour, light);
	
	node->attachObject(object);
	return new Planet(node);
}

void Planet::setMoon(Planet* moon, string name) {
	SceneNode* node = mNode->createChildSceneNode();
	node->attachObject(moon->mNode->detachObject(name));
	moon->mNode = NULL;
	moon->mNode = node;

}

ManualObject * Planet::createSphere(SceneManager& mSceneMgr, const std::string& strName, const float r, ColourValue color, bool light) {
	ManualObject * manual = mSceneMgr.createManualObject(strName);
	
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(strName, "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(light);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(color);

	manual->begin(strName, RenderOperation::OT_TRIANGLE_LIST);
	manual->colour(color);

	const int nRings = 16; const int nSegments = 16;
	float fDeltaRingAngle = (Math::PI / nRings);
	float fDeltaSegAngle = (2 * Math::PI / nSegments);
	unsigned short wVerticeIndex = 0;

	for (int ring = 0; ring <= nRings; ring++) {
		float r0 = r * sinf(ring * fDeltaRingAngle);
		float y0 = r * cosf(ring * fDeltaRingAngle);

		for (int seg = 0; seg <= nSegments; seg++) {
			float x0 = r0 * sinf(seg * fDeltaSegAngle);
			float z0 = r0 * cosf(seg * fDeltaSegAngle);

			manual->position(x0, y0, z0);
			manual->normal(Vector3(x0, y0, z0).normalisedCopy());
			manual->textureCoord((float)seg / (float)nSegments, (float)ring / (float)nRings);

			if (ring != nRings) {
				manual->index(wVerticeIndex + nSegments + 1);
				manual->index(wVerticeIndex);
				manual->index(wVerticeIndex + nSegments);
				manual->index(wVerticeIndex + nSegments + 1);
				manual->index(wVerticeIndex + 1);
				manual->index(wVerticeIndex);
				wVerticeIndex++;
			}
		};
	}

	manual->end();
	MeshPtr mesh = manual->convertToMesh(strName);
	mesh->_setBounds(AxisAlignedBox(Vector3(-r, -r, -r), Vector3(r, r, r)), false);

	mesh->_setBoundingSphereRadius(r);
	unsigned short src, dest;
	if (!mesh->suggestTangentVectorBuildParams(VES_TANGENT, src, dest)) {
		mesh->buildTangentVectors(VES_TANGENT, src, dest);
	}
	return manual;
}

Vector3 Planet::generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2)
{
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;

	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
}
