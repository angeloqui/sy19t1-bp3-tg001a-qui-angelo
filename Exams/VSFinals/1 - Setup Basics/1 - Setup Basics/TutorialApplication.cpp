/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

using namespace std;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	//mCamera->setPolygonMode(PolygonMode::PM_WIREFRAME);

	const int nPlanets = 4;
	float planetSize[nPlanets] = { 1.5f, 2.5f, 5, 4 };
	int planetLocalRotation[nPlanets] = { 1416, 5832, 24, 25 };
	float revolve[nPlanets] = { 88, 224.7, 365.2, 687 };
	float planetDistancefromSun[nPlanets] = { 57.9, 108.2, 149.6, 227.9 };

	ColourValue color[nPlanets] = {
		ColourValue(0.82f, 0.7f, 0.54f),
		ColourValue(0.93f, 0.9f, 0.67f),
		ColourValue::Blue,
		ColourValue(0.71f, 0.25f, 0.05f)
	};

	string planetName[nPlanets] = {
		"Mercury",
		"Venus",
		"Earth",
		"Mars"
	};

	sun = Planet::createPlanet(*mSceneMgr, 9, ColourValue(1.0f, 1.0f, 0.0f), "Sun", false);
	sun->getNode().translate(Vector3::ZERO);
	sun->setLocalRotationSpeed(30);

	for (int i = 0; i < nPlanets; i++) {
		planets[i] = Planet::createPlanet(*mSceneMgr, planetSize[i], color[i], planetName[i], true);
		planets[i]->setParent(sun);
		planets[i]->getNode().translate(planetDistancefromSun[i] , 0, 0);
		planets[i]->setLocalRotationSpeed(planetLocalRotation[i]);
		planets[i]->setRevolutionSpeed(revolve[i]);
	}

	moon = Planet::createPlanet(*mSceneMgr, 0.5f, ColourValue(0.7f, 0.7f, 0.7f), "Moon", true);
	planets[2]->setMoon(moon, "Moon");
	moon->getNode().translate(Vector3(10, 0, 0));
	moon->setLocalRotationSpeed(69);

	// Lecture: Adding a light source
	Light *pointLight = mSceneMgr->createLight("PointLight");
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	//pointLight->setAttenuation(325, 1.0f, 0.014, 0.0007);
	pointLight->setCastShadows(false);

	// Lecture: Setting ambient light
	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
}

//From http://wiki.ogre3d.org/ManualSphereMeshes
//with few adjustments


bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	sun->Update(evt);
	for (int i = 0; i < 4; i++) {
		planets[i]->Update(evt);
	}
	moon->moonRotate(evt);

	Vector3 movement = Vector3::ZERO;
	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		movement.z += 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_K)) {
		movement.z -= 10;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		movement.x -= 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_L)) {
		movement.x += 10;
	}
	movement *= evt.timeSinceLastFrame;
	//mObject->translate(movement);

	// Rotation
	Degree xRot;
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
		xRot += Degree(45.0f * evt.timeSinceLastFrame);
	}
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
		xRot -= Degree(45.0f * evt.timeSinceLastFrame);
	}

	Degree yRot;
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		yRot -= Degree(45.0f * evt.timeSinceLastFrame);
	}
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
		yRot += Degree(45.0f * evt.timeSinceLastFrame);
	}

	// Rotate the object using x and y rotations
	/*mObject->rotate(Vector3(1.0, 0.0, 0.0), Radian(xRot));
	mObject->rotate(Vector3(0.0, 1.0, 0.0), Radian(yRot));*/
	return true;
}


//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
