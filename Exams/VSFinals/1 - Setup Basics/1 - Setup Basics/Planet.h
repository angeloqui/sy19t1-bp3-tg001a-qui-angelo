#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;
using namespace std;

class Planet
{
public:
	Planet(SceneNode* node);
	~Planet();

	void Update(const FrameEvent& evt);
	
	void moonRotate(const FrameEvent& evt);

	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour, string str, bool light);

	SceneNode& getNode();
	ManualObject* getObject();

	void setMoon(Planet* moon, string name);

	void setPosition(Vector3 vec);
	void setParent(Planet* parent);
	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

	static ManualObject* object;
	static ManualObject * createSphere(SceneManager& mSceneMgr, const std::string& strName, const float r, ColourValue color, bool light);
	static Vector3 generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2);
private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;

	int xAxisRotation = 1;
	int zAxisRotation = 1;
	
};