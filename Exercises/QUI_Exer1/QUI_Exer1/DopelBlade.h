#pragma once
#include "Skill.h"

class DopelBlade : public Skill {
public: 
	DopelBlade();
	~DopelBlade();
	
	void cast(Character* target, Character* caster);
};
