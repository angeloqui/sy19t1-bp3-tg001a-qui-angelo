#include "Syphon.h"

Syphon::Syphon() {
}


Syphon::~Syphon() {
}

void Syphon::cast(Character* target, Character* caster) {	

	target->curHp -= caster->weapon->damage;
	
	caster->curHp += caster->weapon->damage * .25;
	caster->curMp -= 25;
	
	//Display
	cout << caster->name << " used Syphon! and dealt " << caster->weapon->damage << " damage to " << target->name
		 << " and restored " << caster->weapon->damage * .25 << " health." << endl;
}
