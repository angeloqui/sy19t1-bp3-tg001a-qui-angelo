#pragma once
#include "Skill.h"

class Vengeance : public Skill {
public: 
	Vengeance();
	~Vengeance();
	
	void cast(Character* target, Character* caster);
};
