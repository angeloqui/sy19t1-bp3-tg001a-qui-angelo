#pragma once

#include<string>

using namespace std;

class Character;

class Skill
{
public:
	Skill();
	~Skill();

	virtual void cast(Character* target, Character* caster);
};
