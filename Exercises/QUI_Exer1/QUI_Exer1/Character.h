#pragma once

#include <string>
#include <vector>

#include "Weapon.h"

//Skills
#include "Attack.h"
#include "Syphon.h"
#include "Vengeance.h"
#include "DopelBlade.h"

using namespace std;

class Skill;

class Character
{
public:
	Character(string name, int maxHp, int maxMp, Weapon* weapon);
	~Character();

	string name;
	int curHp;
	int maxHp;
	int curMp;
	int maxMp;

	int cloneSize;

	Weapon* weapon;
	vector<Skill*> skill;
};

