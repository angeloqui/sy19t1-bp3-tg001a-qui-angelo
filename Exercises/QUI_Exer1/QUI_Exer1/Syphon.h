#pragma once
#include "Skill.h"

class Syphon : public Skill {
public: 
	Syphon();
	~Syphon();
	
	void cast(Character* target, Character* caster);
};
