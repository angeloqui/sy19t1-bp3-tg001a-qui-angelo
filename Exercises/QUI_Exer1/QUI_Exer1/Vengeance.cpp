#include "Vengeance.h"

Vengeance::Vengeance() {
}


Vengeance::~Vengeance() {
}

void Vengeance::cast(Character* target, Character* caster) {
	
	//Display
	cout << caster->name << " used Vengeance! and dealt " << caster->curHp * 0.25 * 2 + caster->weapon->damage << " damage to " << target->name
		 << " and dealt " << caster->curHp * .25 << " damage to self." << endl;
	//
	
	target->curHp -= caster->curHp * 0.25 * 2 + caster->weapon->damage;
	caster->curHp -= caster->curHp * .25;
	
	if (caster->curHp <= 0)
		caster->curHp = 1;

	caster->curMp -= 25;
}
