#pragma once

#include<iostream>
#include<string>

using namespace std;

class Weapon
{
public:
	Weapon(string name, int damage);
	~Weapon();

	string name;
	int damage;
};