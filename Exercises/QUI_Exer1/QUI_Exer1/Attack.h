#pragma once
#include "Skill.h"

class Attack : public Skill {
public: 
	Attack();
	~Attack();
	
	void cast(Character* target, Character* caster);
};
