#include "Character.h"

Character::Character(string name, int maxHp, int maxMp, Weapon* weapon) {
	this->name = name;
	this->maxHp = maxHp;
	this->maxMp = maxMp;
	this->weapon = weapon;

	curHp = maxHp;
	curMp = maxMp;
	cloneSize = 0;
	
	skill.push_back(new Attack());
	skill.push_back(new Syphon());
	skill.push_back(new Vengeance());
	skill.push_back(new DopelBlade());
}

Character::~Character() {
	delete weapon;
}
