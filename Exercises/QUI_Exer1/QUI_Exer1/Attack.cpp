#include "Attack.h"

Attack::Attack() {
}


Attack::~Attack() {
}

void Attack::cast(Character* target, Character* caster) {	

	int finalDamage = caster->weapon->damage;
	
	cout << caster->name << " used Attack";
	
	//Critical Hit
	if ((rand() % 100 + 1) < 25) {
		finalDamage *= 2;
		cout << " [Critical]";
	}
	
	target->curHp -= finalDamage;
	
	//Display
	cout << "! and dealt " << finalDamage << " damage to " << target->name << "." << endl;
	
}
