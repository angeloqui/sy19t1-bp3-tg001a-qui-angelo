#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <string>

#include "Character.h"

//for Dev-C++, remove for VS
/*
#include "Character.cpp"
#include "Weapon.cpp"
#include "Weapon.h"
#include "Skill.cpp"
#include "Skill.h"
#include "Attack.cpp"
#include "Attack.h"
#include "Syphon.cpp"
#include "Syphon.h"
#include "Vengeance.cpp"
#include "Vengeance.h"
#include "DopelBlade.cpp"
#include "DopelBlade.h"
*/

using namespace std;

void displayNormalizedBar(float min, float max) {

	float normalizedValue = min / max;

	while (normalizedValue > 0) {
		cout << "\xfe";
		normalizedValue -= 0.1;
	}
}

int main() {

	srand(time(NULL));

	Character* player[2];
	player[0] = new Character("Player 1", 100, 100, new Weapon("Sword", 10));
	player[1] = new Character("Player 2", 100, 100, new Weapon("Sword", 10));

	Character* clone[2][99];
	
	bool over = false;

	while (true) {
		system("cls");

		//User Interface
		for (int i = 0; i < 2; i++) {
			cout << "[" << player[i]->name << "] " << "(" << player[i]->cloneSize << ") ";
			cout << "\nHP: ";
			displayNormalizedBar(player[i]->curHp, player[i]->maxHp);
			cout << "\nMP: ";
			displayNormalizedBar(player[i]->curMp, player[i]->maxMp);
			cout << endl;
		}

		//Battle
		cout << "\nBattle Log:" << endl;
		for (int i = 0; i < 2; i++) {
			int rng = 3;
			int target = 0;
			if (target == i) target = 1;
			
			//Check if player have enough mana
			if (player[i]->curMp >= 25) 
				 player[i]->skill[rng]->cast(player[target], player[i]);
			else player[i]->skill[0]->cast(player[target], player[i]);
			
			//Create Illusion
			if (rng == 3) {
				clone[i][player[i]->cloneSize] = new Character(player[i]->name + " Clone", player[i]->curHp, player[i]->curMp, new Weapon("Sword", 10));
				player[i]->cloneSize++;
			}

			//Clone
			if (player[i]->cloneSize != 0) {
				for (int j = 0; j < player[i]->cloneSize; j++) {
					rng = rand() % 4;
					if (clone[i][j]->curMp >= 25) 
				 		clone[i][j]->skill[rng]->cast(player[target], clone[i][j]);
					else clone[i][j]->skill[0]->cast(player[target], clone[i][j]);
					
					if (rng == 3) {
						clone[i][player[i]->cloneSize] = new Character(player[i]->name + " Clone", clone[i][j]->curHp, clone[i][j]->curMp, new Weapon("Sword", 10));
						player[i]->cloneSize++;
					}
				}	
			}
			cout << endl;
			
			if (player[target]->curHp <= 0) {
				cout << "\n\n" << player[i]->name << " Wins!";
				over = true;
				break;
			}
		}

		if (over == true) break;

		//Round Over
		_getch();
	}

	//Cleanup
	for (int i = 0; i < 2; i++)	
		delete player[i];
	for (int i = 0; i < 2; i++) 
		for (int j = 0; j < 99; j++)
			delete clone[i][j];
	system("pause");
	return 0;
}
