/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *object = mSceneMgr->createManualObject("object1");
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	
	const int n = 12 * 3;
	int tri[n][3] = {
		{10, 10, 10},
		{-10, 10, 10},
		{-10, -10, 10},

		{10, 10, 10},
		{-10, -10, 10},
		{10, -10, 10},

		{10, 10, -10},
		{10, 10, 10},
		{10, -10, 10},

		{10, 10, -10},
		{10, -10, 10},
		{10, -10, -10},

		{-10, 10, -10},
		{10, 10, -10},
		{10, -10, -10},

		{-10, 10, -10},
		{10, -10, -10},
		{-10, -10, -10},

		{-10, 10, 10},
		{-10, 10, -10},
		{-10, -10, -10},

		{-10, 10, 10},
		{-10, -10, -10},
		{-10, -10, 10},

		{10, 10, -10},
		{-10, 10, -10},
		{-10, 10, 10},

		{10, 10, -10},
		{-10, 10, 10},
		{10, 10, 10},

		{10, -10, 10},
		{-10, -10, 10},
		{-10, -10, -10},

		{10, -10, 10},
		{-10, -10, -10},
		{10, -10, -10}
	};

	for (int i = 0; i < n; i++) {
		object->position(tri[i][0], tri[i][1], tri[i][2]);
	}
	
	object->end();
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(object);
}

bool TutorialApplication::frameStarted(const Ogre::FrameEvent &evt)
{

	int speed = 1;

	// Key down

	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		cubeNode->translate(0, 0, speed * acceleration);
		pressed = true;
	}
		
	else if (mKeyboard->isKeyDown(OIS::KC_J)) {
		cubeNode->translate(-speed * acceleration, 0, 0);
		pressed = true;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K)) {
		cubeNode->translate(0, 0, -speed * acceleration);
		pressed = true;
	}
		
	else if (mKeyboard->isKeyDown(OIS::KC_L)) {
		cubeNode->translate(speed * acceleration, 0, 0);
		pressed = true;
	}
	else pressed = false;

	if (pressed == true) {
		acceleration += evt.timeSinceLastFrame;
	} else if (pressed == false) {
		acceleration = 1;
	}

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
