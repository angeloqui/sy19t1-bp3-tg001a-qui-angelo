#pragma once
#include<OgreManualObject.h>
#include<OgreSceneNode.h>
#include<OgreSceneManager.h>

using namespace Ogre;

class Planet
{
public:
	Planet();
	Planet(SceneNode *node);
	static Planet* createPlanet(SceneManager &sceneManager, float size, ColourValue colour);


	~Planet();
};

