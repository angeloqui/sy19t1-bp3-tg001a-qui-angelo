/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject* manual = createCube(10.0f);
	
	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mObject->attachObject(manual);
}

ManualObject * TutorialApplication::createCube(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject("manual");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	float zSize = size * 2;
	// define vertex position of index 0..3
	// Note that each time you call position() you start a new vertex. 
	// So you need to call colour everytime you define a vertex
	// See: http://www.ogre3d.org/docs/api/1.9/class_ogre_1_1_manual_object.html
	manual->position(-(size / 2), -(size / 2), 0.0);
	manual->colour(0, 0, 255);
	manual->position((size / 2), -(size / 2), 0.0);
	manual->colour(ColourValue::Blue);
	manual->position((size / 2), (size / 2), 0.0);
	manual->colour(ColourValue::Blue);
	manual->position(-(size / 2), (size / 2), 0.0);
	manual->colour(ColourValue::Blue);
	manual->position(-(size / 2), -(size / 2), -(zSize / 2));
	manual->colour(ColourValue::Green);
	manual->position((size / 2), -(size / 2), -(zSize / 2));
	manual->colour(ColourValue::Green);
	manual->position((size / 2), (size / 2), -(zSize / 2));
	manual->colour(ColourValue::Green);
	manual->position(-(size / 2), (size / 2), -(zSize / 2));
	manual->colour(ColourValue::Green);

	// define usage of vertices by refering to the indexes
	// Front
	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(3);
	manual->index(0);
	manual->index(2);
	// Back
	manual->index(6);
	manual->index(5);
	manual->index(4);
	manual->index(6);
	manual->index(4);
	manual->index(7);
	// Left
	manual->index(3);
	manual->index(4);
	manual->index(0);
	manual->index(3);
	manual->index(7);
	manual->index(4);
	// Right
	manual->index(5);
	manual->index(2);
	manual->index(1);
	manual->index(5);
	manual->index(6);
	manual->index(2);
	// Top
	manual->index(6);
	manual->index(3);
	manual->index(2);
	manual->index(3);
	manual->index(6);
	manual->index(7);
	// Bottom
	manual->index(1);
	manual->index(0);
	manual->index(5);
	manual->index(4);
	manual->index(5);
	manual->index(0);

	manual->end();
	return manual;
}

ManualObject * TutorialApplication::createTriangle()
{
	ManualObject* manual = mSceneMgr->createManualObject("manual");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	manual->position(0.0f, 10.0f, -5.0f);
	manual->colour(ColourValue::Red);
	manual->position(-10.0f, -5.0f, -5.0f);
	manual->colour(ColourValue::Green);
	manual->position(10.0f, -5.0f, -5.0f);
	manual->colour(ColourValue::Blue);

	manual->index(0);
	manual->index(1);
	manual->index(2);

	manual->end();
	return manual;
}





bool TutorialApplication::frameStarted(const Ogre::FrameEvent &evt)
{
	/* =====
	Translation
	===== */

	Vector3 movement = Vector3::ZERO;
	int translationSpeed = 50;

	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		movement.z += translationSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K)) {
		movement.z -= translationSpeed;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		movement.x -= translationSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_L)) {
		movement.x += translationSpeed;
	}

	/* =====
	Rotation
	===== */

	Degree xrot, yrot;
	int value = 30;
	int rotationSpeed = 10;
	int constantRotation = 2;
	
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		xrot = Degree(value);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
		xrot = Degree(-value);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
		yrot = Degree(value);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
		yrot = Degree(-value);
	}

	/* =====
	===== */
	
	cubeNode->yaw(Radian(constantRotation * evt.timeSinceLastFrame));

	cubeNode->rotate(Vector3(0, rotationSpeed, 0), Radian(xrot * evt.timeSinceLastFrame));
	cubeNode->rotate(Vector3(rotationSpeed, 0, 0), Radian(yrot * evt.timeSinceLastFrame));
	cubeNode->translate(movement * evt.timeSinceLastFrame);
	
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
