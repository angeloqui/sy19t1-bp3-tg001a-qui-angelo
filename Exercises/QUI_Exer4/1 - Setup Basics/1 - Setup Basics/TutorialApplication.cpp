/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *object = mSceneMgr->createManualObject("object1");
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	
	const int n = 12 * 3;
	int tri[n][3] = {
		{10, 10, 10},
		{-10, 10, 10},
		{-10, -10, 10},

		{10, 10, 10},
		{-10, -10, 10},
		{10, -10, 10},

		{10, 10, -10},
		{10, 10, 10},
		{10, -10, 10},

		{10, 10, -10},
		{10, -10, 10},
		{10, -10, -10},

		{-10, 10, -10},
		{10, 10, -10},
		{10, -10, -10},

		{-10, 10, -10},
		{10, -10, -10},
		{-10, -10, -10},

		{-10, 10, 10},
		{-10, 10, -10},
		{-10, -10, -10},

		{-10, 10, 10},
		{-10, -10, -10},
		{-10, -10, 10},

		{10, 10, -10},
		{-10, 10, -10},
		{-10, 10, 10},

		{10, 10, -10},
		{-10, 10, 10},
		{10, 10, 10},

		{10, -10, 10},
		{-10, -10, 10},
		{-10, -10, -10},

		{10, -10, 10},
		{-10, -10, -10},
		{10, -10, -10}
	};

	for (int i = 0; i < n; i++) {
		object->position(tri[i][0], tri[i][1], tri[i][2]);
	}
	
	object->end();
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(object);
}

bool TutorialApplication::frameStarted(const Ogre::FrameEvent &evt)
{
	/* =====
	Translation
	===== */

	Vector3 movement = Vector3::ZERO;
	int translationSpeed = 50;

	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		movement.z += translationSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K)) {
		movement.z -= translationSpeed;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		movement.x -= translationSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_L)) {
		movement.x += translationSpeed;
	}

	/* =====
	Rotation
	===== */

	Degree xrot, yrot;
	int value = 30;
	int rotationSpeed = 10;
	int constantRotation = 2;
	
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		xrot = Degree(value);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
		xrot = Degree(-value);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
		yrot = Degree(value);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
		yrot = Degree(-value);
	}

	/* =====
	===== */
	
	cubeNode->yaw(Radian(constantRotation * evt.timeSinceLastFrame));

	cubeNode->rotate(Vector3(0, rotationSpeed, 0), Radian(xrot * evt.timeSinceLastFrame));
	cubeNode->rotate(Vector3(rotationSpeed, 0, 0), Radian(yrot * evt.timeSinceLastFrame));
	cubeNode->translate(movement * evt.timeSinceLastFrame);
	
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
