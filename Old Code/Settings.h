#pragma once
#include<string>

using namespace std;

class Settings {
	
public:
	bool isVisible;
	bool hasNPC;
	
	Settings();
	int inputSettings(bool b, string str);
	
	void displayAsciiArt();
};
