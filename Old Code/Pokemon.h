#pragma once
#include<string>

using namespace std;

class Pokemon {

public:
	string name;
	Pokemon* next = NULL;
	
	int baseHP;
	int hp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
	
	string type;
	string type2;
	
	Pokemon();
	Pokemon(Pokemon* pokemon);
	Pokemon(string name, int baseHP, int baseDamage, string type, string type2);
	Pokemon(string name, int baseHP, int baseDamage, string type);	

	void getPokemonList(Pokemon*& pokemon, int i);
	
	// Non Combat
	void scaleStats(int level);
			
	// Combat
	void attack(Pokemon* target);
	void calculateAttack(int& calculatedDamage, Pokemon* target, string str);
	
	void grantExperience(Pokemon* target);
	void displayNormalizedBar(float min, float max);
	
	// Linked List
	void displayPokemons(Pokemon* head);
	void showData(Pokemon* head, int count);
	void healPokemons(Pokemon* head);
	void appendPokemon(Pokemon* head, Pokemon* enemy, int count);
	void switchPokemon(Pokemon*& head, int count);
	void forcedSwitch(Pokemon*& head, bool& isfainted);
};
