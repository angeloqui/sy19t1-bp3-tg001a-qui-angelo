#include<iostream>
#include<string>

//getch
#include<conio.h>

#include"Dialogue.h"

using namespace std;

Dialogue::Dialogue() {
	
	string flavor[] = {
		"Hello, there! \nGlad to meet you!",
		"Welcome to the world of POKeMON!", 
		"My name is OAK.",
		"People affectionately refer to me \nas the POKeMON PROFESSOR.",
		"This world...",
		"...is inhabited far and wide by \ncreatures called POKeMON.",
		"For some people, POKeMON are pets. \nOthers use them for battling.",
		"As for myself...",
		"I study POKeMON as a profession.",
		"But first, tell me a little about \nyourself.",
		"Now tell me. Are you a boy? \nOr are you a girl?",
		
		"Let's begin with your name. \nWhat is it?",
		
		"Right... \nSo your name is ",
		"This is my grandson.",
		"He's been your rival since you both \nwere babies.",
		"....Erm, what was his name now?",
		
		"That's right! I remember now! \nHis name is ",
		"Your very own POKeMON legend is \nabout to unfold!",
		"A world of dreams and adventures \nwith POKeMON awaits! Let's go!"
	};
	
	for (int i = 0; i < (sizeof(flavor)/sizeof(*flavor)); i++)
		this->flavor[i] = flavor[i];
}

void Dialogue::displayFlavorText(bool isVisible, int start, int end) {
	
	if (isVisible == true) {
		for (int i = start; i <= end; i++) {
			system("cls");
			cout << flavor[i];
			_getch();
		}
	}
}

void Dialogue::displayFlavorText(bool isVisible, int i, string str) {
	
	if (isVisible == true) {
		system("cls");
		string temp;
		cout << flavor[i] << str;
		cin >> temp;
	}
}

void Dialogue::displayFlavorText(bool isVisible, string str1, string str2, string str3) {
	
	if (isVisible == true) {
		system("cls");
		cout << str1 << str2 << str3;
		_getch();
	}
}

