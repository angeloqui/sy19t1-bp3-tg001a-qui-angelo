#include<iostream>
#include<string>

#include"Player.h"

//getch
#include<conio.h>

//rand
#include<stdlib.h>
#include<time.h> 

using namespace std;

Player::Player() {}

void Player::getPlayerName(string flavor, string str) {
	
	cout << flavor << str;
	cin >> name;
}

void Player::getPlayerNamePreset(string flavor, string str) {
	
	cout << flavor << str;
	cin >> namePreset;
	
	renamePlayer();
}

void Player::renamePlayer() {
	
	string rivalNames[] = {
		"",
		"GREEN",
		"GARY",
		"KAZ",
		"TORU"
	};
		
	if (namePreset == 0) {
		getPlayerName("", "INPUT: ");
	} else {
		name = rivalNames[namePreset];
	}
}

void Player::selectStarter() {
		
	cout << "Select a Starter Pokemon: \n\n[1] Bulbasaur \n[2] Squirtle \n[3] Charmander \n\nINPUT: ";
	cin >> select;
}

void Player::showUserInterface() {
	cout << "X: " << x << ", Y: " << y;
	cout << "\nLocation: " << location;
	cout << "\nPOKeMON Dollars: " << money;
}

void Player::Main(int& panel, bool hasNPC, Player* NPC) {
	
	cout << "\n\n[1] Move \n[2] Display Pokemon Data \n[3] POKeMON Center \n[4] Poke Mart";
	
	if (hasNPC == true)
		cout << "\n[5] Battle " << NPC->name;
				
	cout << "\n\nINPUT: ";
	cin >> panel;
	
	// Battle with rival, move to battle panel
	if (panel == 5) {
		inCombat = true;
	}
}

void Player::Move(int& panel) {	
	
	cout << "\n\n[1] Up \n[2] Down \n[3] Left \n[4] Right \n\nINPUT (0 to return): ";
	cin >> select;
	
	// 0 to return to Main, else move to coordinate vector
	if (select == 0) 
		panel = 0;
	else {
		// Up
		if (select == 1) {
			if (this->y == 6) {
				cout << "Out of Bounds";
				_getch();
			} else 
				this->y += 1; 
		}
	
		// Down
		else if (select == 2) {
			if (this->y == 0) {
				cout << "Out of Bounds";
				_getch();
			} else 
				this->y -= 1;
		}
	
		// Left
		else if (select == 3) {
			if (this->x == 0) {
				cout << "Out of Bounds";
				_getch();
			} else 
				this->x -= 1;
		}
	
		// Right
		else if (select == 4 ) {
			if (this->x == 9) {
				cout << "Out of Bounds";
				_getch();
			} else 
				this->x += 1;
		}
		
		// If not in town, has a chance to encounter a pokemon
		if (inTown != 1) {
			if (rand() % 100 + 1 <= 90) {
				inCombat = true;
			}
		}
	}
}

void Player::Data(int& panel) {
	
	// Display Pokemon/s (minor detail)
	cout << "\n\nSelect Pokemon: ";
	pokemon->displayPokemons(pokemon);
			
	cout << "\n\nINPUT (0 to return): ";
	cin >> select;
			
	// 0 to return to Main, else show specific pokemon's full detail 
	if (select == 0) {
		
		if (panel == 1 || panel == 2) {
			panel = 0;	
		} else if (panel == 5) {
			panel = 5;
		}	
	}
	else {
		int i = select;
		pokemon->showData(pokemon, i - 1);
		_getch();
		
		cout << "\n\nSwap with " << pokemon->name << " ? \n\n[0] No \n[1] Yes \n\nINPUT: ";
		cin >> select;
		
		if (select == 1)
			pokemon->switchPokemon(pokemon, i);
	}
}

void Player::Heal(int& panel) {
	
	if (inTown == true) {
		cout << "\n\nWould you like me to heal your POKeMON back to perfect health? \n\n[0] No \n[1] Yes \n\nINPUT: ";
		cin >> select;
			
		if (select == 1) {
			cout << "Okay, I'll take your POKeMON for a few seconds.";
			_getch();
			pokemon->healPokemons(pokemon);
			cout << "\nWe've restored your POKeMON to full health.";
			_getch();
		}
	} 
	else {
		cout << "\n\nYou are not in town!";
		_getch();
	}
	panel = 0;
}

void Player::Shop(int& panel) {
		
	if (inTown == true) {			
		cout << "\nPokeballs: " << pokeballs;
				
		cout << "\n\nBUY POKEBALLS? (100) \n[0] No \n[1] Yes \n\nINPUT: ";
		cin >> select;
				
		if (select == 1) {
			if (money >= 100) {
				pokeballs++;
				money -= 100;
			} 
			else {
				cout << "Insufficient Funds";
				_getch();
			}
		} else panel = 0;
	} 
	else {
		cout << "\n\nYou are not in town!";
		_getch();
		panel = 0;
	}
}

void Player::Battle(int& panel, Pokemon*& enemy) {
	
	cout << "\n\nWild " << enemy->name << " appeared!";
	_getch();
	
	while (true) {
		
		system("cls");
		
		//Encountered Pokemon UI
		cout << "\n\n " << enemy->name;
		cout << "\n Level: " << enemy->level;
		cout << "\n HP: ";
		enemy->displayNormalizedBar(enemy->hp, enemy->baseHP);
				
		//Player->Pokemon UI
		cout << "\n\n\n\t\t\t" << pokemon->name;
		cout << "\n\t\t\tLevel: " << pokemon->level;
		cout << "\n\t\t\tHP:  ";
		pokemon->displayNormalizedBar(pokemon->hp, pokemon->baseHP);
		cout << "\n\t\t\tEXP: ";
		pokemon->displayNormalizedBar(pokemon->exp, pokemon->expToNextLevel);
		
		//Player UI
		cout << "\n\n[1] ATTACK \n[2] POKeMON \n[3] POKEBALL/S(" << pokeballs << ") \n[4] RUN \n\nINPUT: ";
		cin >> select;
		
		//Attack
		if (select == 1) {
			
			//Player Turn
			if (pokemon->hp > 0)
				pokemon->attack(enemy);

			_getch();
			
			//Enemy Turn
			if (enemy->hp > 0) {
				enemy->attack(pokemon);	
				
				if (pokemon->hp <= 0) {
					pokemon->forcedSwitch(pokemon, isfainted);
					
					if (isfainted == true) {
						x = 0;
						y = 0;
						cout << "\n" << name << " blacked out!";
						panel = 0;
					}					
				}
			} 
			else {
				pokemon->grantExperience(enemy);
			
				if (isNpcBattle == true) {
					
					srand (time(NULL));
					int random = rand() % 1000 + 1;
					
					money += random;
					
					cout << "\nYou recieved $" << random << "!";
					
					if (npcBattleCount < 8) 
						npcBattleCount++;
				}
			}
		_getch();
		}
		
		// POKeMons
		if (select == 2) {
			Data(panel);
		}
		
		// Capture
		if (select == 3 && isNpcBattle == false) {
			if (noOfPokemons < 6) {
				if (pokeballs > 0) {
					pokeballs--;
							
					if (rand() % 100 + 1 < 30) {
						cout << "\nGotcha! " << enemy->name << " was caught!";
						noOfPokemons++;
						pokemon->appendPokemon(pokemon, enemy, noOfPokemons);
						inCombat = false;
						_getch();
						panel = 0;
						break;
					} else {
						cout << "\nOh, no! The POKeMON broke free!";
						enemy->attack(pokemon);
					}
				}
			} 
			else {
				cout << "\nNumber of POKeMONs reached, try a new game! (Moving POKeMON to box not yet implemented)";
			}
		_getch();
		}
			
		// Run
		else if (select == 4) {
			isNpcBattle = false;
			inCombat = false;
			break;
		}
		
		//Wins
		if (pokemon->hp <= 0 || enemy->hp <= 0) {	
			isNpcBattle = false;
			inCombat = false;
			break;
		}
	}
	if (panel != 0) panel = 1;
}
