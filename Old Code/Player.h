#pragma once
#include <string>

#include"Pokemon.h"
#include"Location.h"

using namespace std;

class Player {

public:
	// Init
	string name;
	int namePreset;
	
	Player();
	
	void getPlayerName(string flavor, string str);
	void getPlayerNamePreset(string flavor, string str);
	void renamePlayer();
	
	// Game
	Pokemon* pokemon;
	void selectStarter();
	int select;
	
	// Game Loop
	int x = 0, y = 0;
	string location;
	bool inTown;
	bool inCombat;
	bool isfainted;
	
	int pokeballs = 10;
	int noOfPokemons = 1;
	int money = 500;
	
	bool isNpcBattle = 0;
	
	void showUserInterface();
	
	// Pokemon Panel Names
	void Main(int& panel, bool hasNPC, Player* NPC);
	void Move(int& panel);
	void Data(int& panel);
	void Heal(int& panel);
	void Shop(int& panel);
	void Battle(int& panel, Pokemon*& enemy);
	
	//NPC
	int npcBattleCount = 0;
	int npcLevel[9] = { 5, 9, 18, 20, 25, 40, 53, 63, 75 };
};
