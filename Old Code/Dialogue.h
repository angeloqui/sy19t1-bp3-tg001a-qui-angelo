#pragma once
#include<string>

#include"Dialogue.h"

using namespace std;

class Dialogue {
	
public:
	string flavor[19];
	
	Dialogue();
	
	void displayFlavorText(bool isVisible, int start, int end); // display an array of flavor text
	void displayFlavorText(bool isVisible, int i, string str);  // flavor text with user input (does not store)
	void displayFlavorText(bool isVisible, string str1, string str2, string str3); //flavor text with string var in-between
};
