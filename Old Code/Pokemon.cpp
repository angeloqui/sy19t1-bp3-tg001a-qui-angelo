#include<iostream>
#include<string>

#include"Pokemon.h"

using namespace std;

Pokemon::Pokemon() {}

Pokemon::Pokemon(string name, int baseHP, int baseDamage, string type)
{
	this->name = name;
	this->baseHP = baseHP;
	this->hp = baseHP;
	this->level = 1;
	this->baseDamage = baseDamage;
	this->exp = 0;
	this->expToNextLevel = 10;
	this->type = type;
}

Pokemon::Pokemon(string name, int baseHP, int baseDamage, string type, string type2)
{
	this->name = name;
	this->baseHP = baseHP;
	this->hp = baseHP;
	this->level = 1;
	this->baseDamage = baseDamage;
	this->exp = 0;
	this->expToNextLevel = 10;
	this->type = type;
	this->type2 = type2;
}

void Pokemon::getPokemonList(Pokemon*& newPokemon, int i) {
	
	Pokemon* pokemonList[15];
	pokemonList[0] = new Pokemon("Bulbasaur", 45, 49, "Grass", "Poison");
	pokemonList[1] = new Pokemon("Squirtle", 44, 48, "Water");
	pokemonList[2] = new Pokemon("Charmander", 39, 52, "Fire");
	pokemonList[3] = new Pokemon ("Caterpie", 45, 30, "Bug");
	pokemonList[4] = new Pokemon ("Weedle", 40, 35, "Bug", "Poison");
	pokemonList[5] = new Pokemon ("Pidgey", 40, 45, "Normal", "Flying");
	pokemonList[6] = new Pokemon ("Nidoran F", 55, 47, "Poison");
	pokemonList[7] = new Pokemon ("Nidoran M", 46, 57, "Poison");
	pokemonList[8] = new Pokemon ("Oddish", 45, 50, "Grass", "Poison");
	pokemonList[9] = new Pokemon ("Poliwag", 40, 50, "Water");
	pokemonList[10] = new Pokemon ("Abra", 25, 20, "Psychic");
	pokemonList[11] = new Pokemon ("Machop", 70, 80, "Fighting");
	pokemonList[12] = new Pokemon ("Bellsprout", 50, 75, "Grass", "Poison");
	pokemonList[13] = new Pokemon ("Geodude", 40, 80, "Fighting");
	pokemonList[14] = new Pokemon ("Ghastly", 30, 35, "Ghost", "Poison");
	
	newPokemon = pokemonList[i];
}

void Pokemon::scaleStats(int level) {
	
	for (int i = 1; i < level; i++) {
		this->baseHP += (int)(baseHP * 0.15);
		this->hp = baseHP;
		this->baseDamage += (int)(baseDamage * 0.10);
		this->expToNextLevel += (int)(expToNextLevel * 0.20);
	}
}

void Pokemon::displayNormalizedBar(float min, float max) {
	
	float normalizedValue = min / max;

	while (normalizedValue > 0) {
		cout << "\xfe";
		normalizedValue -= 0.1;
	}
}

//Battle
void Pokemon::attack(Pokemon* target) {
	
	if (rand() % 100 + 1 > 20) {
		
		//Type
		int calculatedDamage = baseDamage;
		calculateAttack(calculatedDamage, target, "Bug");

		
		
		
		
		target->hp -= this->baseDamage;
		

		cout << "\n" << this->name << " dealt " << this->baseDamage << " damage to " << target->name;
	} else {
		cout << "\n" << this->name << " missed!";
	}
	
	if (target->hp < 0) {
		target->hp = 0; //Enemy health clamp to 0
		cout << "... " << target->name << " fainted!";
	} 
}

void Pokemon::calculateAttack(int& calculatedDamage, Pokemon* target, string str) {
	
		//if (target->type == "Bug" || target->type2 == "Bug")
}
	

void Pokemon::grantExperience(Pokemon* target) {
	
	int exp = target->level * 10;
	for (int i = 1; i < target->level; i++) {
		exp += (int)(target->level * .2);
	}
	
	cout << "\n" << this->name << " recieved " << exp << " experience!";
	this->exp += exp;
	
	//Level Up
	while (this->exp >= this->expToNextLevel) {
		this->level++;
		this->exp -= this->expToNextLevel;

		this->scaleStats(2);
		this->hp = this->baseHP;
		
		cout << "\n" << this->name << " grew to LV. " << this->level;
		cout << "\nMAX. HP: " << this->baseHP;
		cout << "\nATTACK: " << this->baseDamage;
 	}
}

//Linked List
 void Pokemon::displayPokemons(Pokemon* head) {
 	
	int counter = 1;
	
	Pokemon* currentPokemon = head;
	
	do {
		cout << "\n" << counter << ". " << currentPokemon->name;
		
		//Abra is messing up the alignment
		if (currentPokemon->name == "Abra") {
			cout << "\t";
		}
		
		cout << "\tLv: " << currentPokemon->level << "\tHP: ";
	
		displayNormalizedBar(currentPokemon->hp, currentPokemon->baseHP);
		currentPokemon = currentPokemon->next;
		counter++;
	}
	while(currentPokemon != head);
}

void Pokemon::showData(Pokemon* head, int count) {
	
	Pokemon* currentPokemon = head;
	
	for (int i = 0; i < count; i++)
		currentPokemon = currentPokemon->next;
	
	cout << "\nName: " << currentPokemon->name;
	cout << "\nHP: " << currentPokemon->hp << "/" << currentPokemon->baseHP;
	cout << "\nLevel: " << currentPokemon->level;
	cout << "\nBase Damage: " << currentPokemon->baseDamage;
	cout << "\nExp: " << currentPokemon->exp << "/" << currentPokemon->expToNextLevel;
}

 void Pokemon::healPokemons(Pokemon* head) {
 	
	Pokemon* currentPokemon = head;
	
	do {
		currentPokemon->hp = currentPokemon->baseHP;
		currentPokemon = currentPokemon->next;
	} while(currentPokemon != head);
}

void Pokemon::appendPokemon(Pokemon* head, Pokemon* enemy, int count) {
	
	Pokemon* newPokemon = new Pokemon(enemy);
	newPokemon->next = head;
	
	Pokemon* currentPokemon = head;
	
	for (int i = 1; i < count - 1; i++) {
		currentPokemon = currentPokemon->next; 
	}
	
	currentPokemon->next = newPokemon;
}

Pokemon::Pokemon(Pokemon* enemy) {
	this->name = enemy->name;
	this->baseHP = enemy->baseHP;
	this->hp = enemy->hp;
	this->level = enemy->level;
	this->baseDamage = enemy->baseDamage;
	this->exp = 0;
	this->expToNextLevel = enemy->expToNextLevel;
	this->type = enemy->type;
	this->type2 = enemy->type2;
}

void Pokemon::switchPokemon(Pokemon*& head, int count) {
	
	Pokemon* currentPokemon = head;
	
	for (int i = 1; i < count; i++) {
		currentPokemon = currentPokemon->next;
	}
	
	head = currentPokemon;
}

void Pokemon::forcedSwitch(Pokemon*& head, bool& isfainted) {
	Pokemon* currentPokemon = head;
	
	do {
		currentPokemon = currentPokemon->next;

		if (currentPokemon->hp > 0) {
			break;
		}
	}
	while(currentPokemon != head);
	
	if (currentPokemon->hp <= 0) {
		isfainted = true;
	} else {
		head = currentPokemon;	
	}
}
