#include "Crystal.h"

Crystal::Crystal(string name, int rarity) : Item(name, rarity) {
}


Crystal::~Crystal() {
}

void Crystal::cast(Character * caster) {
	cout << caster->getName() << " pulled " << name << "!" << endl;
	cout << "Adds 15 crystals (effectively, 2 free pulls). "<< endl << endl;
	
	caster->addCrystals(15);
	counter++;
}
