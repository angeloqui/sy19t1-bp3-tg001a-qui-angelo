#pragma once

#include "Item.h"

class RareItemSSR : public Item {
public:
	RareItemSSR(string name, int rarity);
	~RareItemSSR();

	void cast(Character *caster);
};
