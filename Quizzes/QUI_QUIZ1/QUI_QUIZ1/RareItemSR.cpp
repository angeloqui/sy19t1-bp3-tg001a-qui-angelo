#include "RareItemSR.h"

RareItemSR::RareItemSR(string name, int rarity) : Item(name, rarity) {
}

RareItemSR::~RareItemSR() {
}

void RareItemSR::cast(Character * caster) {
	cout << caster->getName() << " pulled " << name << "!" << endl;
	cout << "10 Rarity Point." << endl << endl; 
	
	caster->addRarity(10);
	counter++;
}
