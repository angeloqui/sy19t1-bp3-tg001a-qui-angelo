#pragma once

#include "Item.h"

class Crystal : public Item {
public:
	Crystal(string name, int rarity);
	~Crystal();

	void cast(Character *caster);
};

