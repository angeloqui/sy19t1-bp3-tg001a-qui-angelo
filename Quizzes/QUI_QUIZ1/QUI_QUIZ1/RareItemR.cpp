#include "RareItemR.h"

RareItemR::RareItemR(string name, int rarity) : Item(name, rarity) {
}

RareItemR::~RareItemR() {
}

void RareItemR::cast(Character * caster) {
	cout << caster->getName() << " pulled " << name << "!" << endl;
	cout << "1 Rarity Point." << endl << endl; 
	
	caster->addRarity(1);
	counter++;
}
