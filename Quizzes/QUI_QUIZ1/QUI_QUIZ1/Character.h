#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Item;
class Character {
public:
	Character(string name, int hp, int crystals);
	~Character();

	void Pull();

	void takeDamage(int value);
	void reduceCrystals(int value);
	void addCrystals(int value);
	void heal(int value);
	void addRarity(int value);

	string getName();
	int getHp();
	int getCrystals();
	int getCounter();
	void addCounter();
	int getRarity();
	
	void printStats();
	void printSummary();
	bool isAlive();
	bool hasCrystals();
	
	//Extra
	void printItemRarities(int value);
private:
	string name;
	int hp;
	int crystals;
	int rarityPoints;
	int counter;
	vector<Item*> items;
	vector<Item*> inventory;
};
