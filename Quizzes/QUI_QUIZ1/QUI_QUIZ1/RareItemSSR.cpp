#include "RareItemSSR.h"

RareItemSSR::RareItemSSR(string name, int rarity) : Item(name, rarity) {
}

RareItemSSR::~RareItemSSR() {
}

void RareItemSSR::cast(Character * caster) {
	cout << caster->getName() << " pulled " << name << "!" << endl;
	cout << "50 Rarity Point." << endl << endl; 
	
	caster->addRarity(50);
	counter++;
}
