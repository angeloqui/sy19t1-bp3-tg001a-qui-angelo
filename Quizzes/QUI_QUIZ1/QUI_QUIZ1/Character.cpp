#include "Character.h"
#include "RareItemSSR.h"
#include "RareItemSR.h"
#include "RareItemR.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystal.h"

Character::Character(string name, int hp, int crystals) {
	this->name = name;
	this->hp = hp;
	this->crystals = crystals;
	
	this->rarityPoints = 0;
	
	items.push_back(new RareItemSSR("SSR", 1));
	items.push_back(new RareItemSR("SR", 9));
	items.push_back(new RareItemR("R", 40));
	items.push_back(new HealthPotion("Health Potion", 15));
	items.push_back(new Bomb("Bomb", 20));
	items.push_back(new Crystal("Crystal", 15));
	
	//Calculate the roll chance depending on the second parameter
	//in new Items
	int temp = 1;
	for (int i = 0; i < items.size(); i++) {
		Item *initItem = items[i];
		initItem->setThreshold(temp);
		temp += initItem->getRarity();
	}
}

Character::~Character() {
	for (int i = 0; i < items.size(); i++) delete items[i];
	items.clear();

	for (int i = 0; i < inventory.size(); i++) delete inventory[i];
	inventory.clear();
}

void Character::Pull() {
	reduceCrystals(5);
	addCounter();
	

	//Add item to inventory
	int chance = rand() % 100 + 1;
	printItemRarities(chance);
	for (int i = 0; i < items.size(); i++) {
		Item *itemToUse = items[i];
		if (chance >= itemToUse->getThreshold() && chance < itemToUse->getThreshold() + itemToUse->getRarity()) {
			itemToUse->cast(this);
			inventory.push_back(itemToUse);	
		}
	}
}

void Character::takeDamage(int value) {
	if (value > 0) {
		hp -= value;

		if (hp < 0) hp = 0;
	}
}

void Character::reduceCrystals(int value) {
	if (value > 0) {
		crystals -= value;

		if (crystals < 0) crystals = 0;
	}
}

void Character:: addCrystals(int value) {
	if (value > 0) {
		crystals += value;
	}
}

void Character::heal(int value) {
	if (value > 0) {
		hp += value;
	}
}

void Character::addRarity(int value) {
	if (value > 0) {
		rarityPoints += value;
	}
}

string Character::getName() {
	return this->name;
}

int Character::getHp() {
	return this->hp;
}

int Character::getCrystals() {
	return this->crystals;
}

int Character::	getCounter() {
	return this->counter;
}

void Character:: addCounter() {
	counter++;
}

int Character:: getRarity() {
	return this->rarityPoints;
}

void Character::printStats() {
	cout << "Character: " << name << endl;
	cout << "HP: " << hp << endl;
	cout << "Crystals: " << crystals << endl;
	cout << "Rarity Points: " << rarityPoints << endl << endl;
}

void Character::printSummary() {	
	int counter = 1;
	
	while (counter * 5 < inventory.size() + 5) {
		//Check if win or lose
		if (getRarity() >= 100) 
			cout << name << " Wins!" << endl << endl;
		else 
			cout << name << " Loses!" << endl << endl;
		
		//Print previous stats
		printStats();
		
		//Display items using counters (Collate)
		for (int i = 0; i < items.size(); i++) {
			cout << items[i]->getName() << " x" << items[i]->getCounter() << endl;
		}
		
		//Display items using inventory model
		cout << "\nPage: " << counter << endl;
		cout << "Items: ";
		if (counter * 5 < inventory.size()) cout << counter * 5;
		else cout << inventory.size(); 
		cout << " / " << inventory.size() << endl;
		cout << string(20, '=') << endl;	
		
		for (int i = counter * 5 - 5; i < counter * 5; i++) {
			if (i == inventory.size()) break;
			cout << i + 1 << ". " << inventory[i]->getName() << endl;
		}

		counter++;
		cout << endl;
		system("pause");
		system("cls");
	}
}

bool Character::isAlive() {
	return hp > 0;
}

bool Character::hasCrystals() {
	return crystals > 0;
}

void Character::printItemRarities(int roll) {
	//Print number of turns made and round roll value.
	cout << " Turns: " << counter << "\t\tRolled: " << roll << endl;
	
	//Print additional information - roll chance to get item
	cout << string(35, '=') << endl;
	for (int i = 0; i < items.size(); i++) {
		Item *itemDisplay = items[i];
		cout << " " << itemDisplay->getName() << "\t\t";
		if (itemDisplay->getName().size() <= 6) cout << "\t"; 
		cout << itemDisplay->getThreshold() << " - " << itemDisplay->getThreshold() + itemDisplay->getRarity() - 1 << endl; 
	}
	cout << endl;
}