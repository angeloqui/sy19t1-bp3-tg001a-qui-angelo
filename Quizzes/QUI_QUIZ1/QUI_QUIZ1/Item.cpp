#include "Item.h"

Item::Item(string name, int rarity) {
	this->name = name;
	this->rarity = rarity;
	counter = 0;
}

Item::~Item() {
}

void Item::cast(Character * caster) {
}

string Item::getName() {
	return name;
}

int Item::getRarity() {
	return rarity;
}

int Item::getCounter() {
	return counter;
}

int Item::getThreshold() {
	return threshold;
}

void Item::setThreshold(int value) {
	threshold = value;
} 
