#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Character.h"

using namespace std;

int main() {
	
	srand(time(NULL));
	
	Character *player = new Character("Player", 100, 100);
	
	while (player->isAlive() && player->hasCrystals()) {		
		player->printStats();
		player->Pull();
		
		system("pause");
		system("cls");
	}
	
	player->printSummary();
	//delete player;
	return 0;
}