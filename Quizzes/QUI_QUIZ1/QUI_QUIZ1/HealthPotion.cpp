#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int rarity) : Item(name, rarity) {
}


HealthPotion::~HealthPotion() {
}

void HealthPotion::cast(Character * caster) {
	cout << caster->getName() << " pulled " << name << "!" << endl;
	cout << "Heals 30 HP" << endl << endl;
	
	caster->heal(30);
	counter++;
}
