#pragma once

#include "Item.h"

class RareItemR : public Item {
public:
	RareItemR(string name, int rarity);
	~RareItemR();

	void cast(Character *caster);
};
