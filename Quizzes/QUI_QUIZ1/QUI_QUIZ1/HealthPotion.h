#pragma once

#include "Item.h"

class HealthPotion : public Item {
public:
	HealthPotion(string name, int rarity);
	~HealthPotion();

	void cast(Character *caster);
};

