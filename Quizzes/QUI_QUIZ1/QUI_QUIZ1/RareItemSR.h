#pragma once

#include "Item.h"

class RareItemSR : public Item {
public:
	RareItemSR(string name, int rarity);
	~RareItemSR();

	void cast(Character *caster);
};
