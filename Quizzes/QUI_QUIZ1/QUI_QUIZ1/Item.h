#pragma once

#include "Character.h"
#include <string>

using namespace std;

class Item {
public:
	Item(string name, int rarity);
	~Item();
	
	virtual void cast(Character *caster);

	string getName();
	int getRarity();
	int getCounter();
	int getThreshold();
	void setThreshold(int value);
protected:
	string name;
	int rarity;
	int counter;
	int threshold = 0;
};
