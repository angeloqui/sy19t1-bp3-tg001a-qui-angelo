#include "Bomb.h"


Bomb::Bomb(string name, int rarity) : Item(name, rarity) {
}


Bomb::~Bomb() {
}

void Bomb::cast(Character * caster) {
	cout << caster->getName() << " pulled " << name << "!" << endl;
	cout << "Deals 25 damage. "<< endl << endl;
	
	caster->takeDamage(25);
	counter++;
}
