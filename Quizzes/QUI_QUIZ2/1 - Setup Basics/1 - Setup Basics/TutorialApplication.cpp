/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include "Planet.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void) {

	const int nPlanets = 4;
	float planetSize[nPlanets] = { 1.5f, 2.5f, 5, 4 };
	int planetLocalRotation[nPlanets] = { 1408, 5832, 24, 25 };
	float revolve[4] = { 88, 224, 365, 687 };
	int planetDistancefromSun[nPlanets] = { 57, 108, 149, 227 };

	ColourValue color[nPlanets] = {
		ColourValue::ColourValue(0.59f, 0.29f, 0.0f, 1.0f),
		ColourValue::ColourValue(1.0f, 0.71f, 0.76f, 1.0f),
		ColourValue::Blue,
		ColourValue::Red
	};

	string planetName[nPlanets] = {
		"Mercury",
		"Venus",
		"Earth",
		"Mars"
	};
	
	sun = Planet::createPlanet(*mSceneMgr, 20, ColourValue::ColourValue(1.0f, 1.0f, 0.0f, 1.0f), "Sun");
	sun->setLocalRotationSpeed(27);
	
	planet[nPlanets];
	for (int i = 0; i < 4; i++) {
		planet[i] = Planet::createPlanet(*mSceneMgr, planetSize[i], color[i], planetName[i]);
		planet[i]->setParent(sun);

		planet[i]->getNode().setPosition(planetDistancefromSun[i], 0, 0);
		planet[i]->setRevolutionSpeed(revolve[i]);
		planet[i]->setLocalRotationSpeed(planetLocalRotation[i]);
	}

	moon = Planet::createPlanet(*mSceneMgr, 0.5f, ColourValue::White, "Moon");
	moon->setLocalRotationSpeed(69);
	moon->setParent(planet[2]);
	planet[2]->setMoon(moon, "Moon");
	moon->getNode().setPosition(10, 0, 0);
}

bool TutorialApplication::frameStarted(const Ogre::FrameEvent &evt) {
	
	sun->Update(evt);
	moon->moonRotate(evt);
	for (int i = 0; i < 4; i++) 
		planet[i]->Update(evt);
	

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
