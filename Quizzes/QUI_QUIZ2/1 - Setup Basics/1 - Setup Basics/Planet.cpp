#include "Planet.h"
ManualObject* Planet::object = NULL;

Planet::Planet(SceneNode * node) {
	this->mNode = node;
}

Planet::~Planet(){
}

void Planet::Update(const FrameEvent& evt) {
	float rotSpd = 24 / mLocalRotationSpeed;
	float revSpd = (365 / mRevolutionSpeed);
	Degree rot = Degree(60 * evt.timeSinceLastFrame * rotSpd);
	mNode->rotate(Vector3(0, 1, 0), Radian(rot));
	if (mParent != NULL ) {
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * revSpd);
		Vector3 location = Vector3::ZERO;
		location.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		location.z = mNode->getPosition().z - mParent->mNode->getPosition().z;
		float oldX = location.x;
		float oldZ = location.z;
		float newX = (oldX*Math::Cos(planetRevolution)) + (oldZ*Math::Sin(planetRevolution));
		float newZ = (oldX*-Math::Sin(planetRevolution)) + (oldZ*Math::Cos(planetRevolution));
		mNode->setPosition(newX, mNode->getPosition().y, newZ);
	}
}

void Planet::moonRotate(const FrameEvent& evt) {
	float rotSpd = 24 / mLocalRotationSpeed;
	float revSpd = (365 / mRevolutionSpeed);
	Degree rot = Degree(60 * evt.timeSinceLastFrame * rotSpd);
	mNode->rotate(Vector3(0, 1, 0), Radian(rot));
}

SceneNode & Planet::getNode() {
	return *mNode;
}

ManualObject* Planet::getObject() {
	return object;
}

void Planet::setPosition(Vector3 vec)
{
	mNode->setPosition(vec);
}

void Planet::setParent(Planet * parent) {
	mParent = parent;
}

void Planet::setLocalRotationSpeed(float speed) {
	 mLocalRotationSpeed = speed;
}	

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}


Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour, string str) {
	SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode(str);
	object = sceneManager.createManualObject(str);
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

		const int n = 12 * 3;
		int tri[n][3] = {
			{size, size, size},
			{-size, size, size},
			{-size, -size, size},

			{size, size, size},
			{-size, -size, size},
			{size, -size, size},

			{size, size, -size},
			{size, size, size},
			{size, -size, size},

			{size, size, -size},
			{size, -size, size},
			{size, -size, -size},

			{-size, size, -size},
			{size, size, -size},
			{size, -size, -size},

			{-size, size, -size},
			{size, -size, -size},
			{-size, -size, -size},

			{-size, size, size},
			{-size, size, -size},
			{-size, -size, -size},

			{-size, size, size},
			{-size, -size, -size},
			{-size, -size, size},

			{size, size, -size},
			{-size, size, -size},
			{-size, size, size},

			{size, size, -size},
			{-size, size, size},
			{size, size, size},

			{size, -size, size},
			{-size, -size, size},
			{-size, -size, -size},

			{size, -size, size},
			{-size, -size, -size},
			{size, -size, -size}
		};

		for (int i = 0; i < n; i++) {
			object->position(tri[i][0], tri[i][1], tri[i][2]);
			object->colour(colour);
		}

		object->end();
		node->attachObject(object);
		return new Planet(node);
}

void Planet::setMoon(Planet* moon, string name) {
	SceneNode* node = mNode->createChildSceneNode();
	node->attachObject(moon->mNode->detachObject(name));
	moon->mNode = NULL;
	moon->mNode = node;

}
