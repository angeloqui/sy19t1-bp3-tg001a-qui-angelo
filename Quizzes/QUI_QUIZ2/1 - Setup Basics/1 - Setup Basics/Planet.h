#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;
using namespace std;

class Planet
{
public:
	Planet(SceneNode* node);
	~Planet();

	void Update(const FrameEvent& evt);
	
	void moonRotate(const FrameEvent& evt);

	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour, string str);

	SceneNode& getNode();
	ManualObject* getObject();

	void setMoon(Planet* moon, string name);

	void setPosition(Vector3 vec);
	void setParent(Planet* parent);
	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

	static ManualObject* object;

private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;

	int xAxisRotation = 1;
	int zAxisRotation = 1;
};